import library.Book;
import library.Category;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class BookTest {
    @Test
    public void shouldCreateBook(){

        Book book = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        book.setBorrowExpirationDate(LocalDate.of(2001,12,12));
        assertEquals(book.getCategory(), Category.NOVEL);
        assertEquals(book.getTitle(), "TEST");
        assertEquals(book.getBorrowExpirationDate(), LocalDate.of(2001,12,12));

    }

}