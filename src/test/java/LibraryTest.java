import library.Book;
import library.Category;
import library.Client;
import library.Library;
import org.junit.Assert;
import org.junit.Test;
import tools.Boot;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class LibraryTest {

    @Test
    public void shouldAddBookToLibary(){
        Book book = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Book book2 = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Library library = new Library();

        library.addBook(book);

        Assert.assertFalse(library.getBookList().isEmpty());
        Assert.assertEquals(book, library.getBookList().get(0));
    }
    @Test
    public void shouldNOTaddBook(){
        Book book = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Book book2 = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Library library = new Library();

        library.addBook(book);
        library.addBook(book);

        Assert.assertFalse(library.getBookList().isEmpty());
        Assert.assertEquals(book, library.getBookList().get(0));
        Assert.assertEquals(library.getBookList().size(), 1);
    }
    @Test
    public void shouldRemoveBook(){
        Book book = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Library library = new Library();

        library.addBook(book);
        library.removeBook(book);

        Assert.assertTrue(library.getBookList().isEmpty());
    }
    @Test
    public void shouldAddClient(){
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        Library library = new Library();

        library.addClient(client);

        Assert.assertFalse(library.getClientList().isEmpty());
    }
    @Test
    public void shouldRemoveClient(){
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        Library library = new Library();

        library.addClient(client);
        library.removeClient(client);

        Assert.assertTrue(library.getClientList().isEmpty());

    }
    @Test
    public void  shouldShowLIstOfClients(){
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        Client client2 = new Client("Mateusz","Nowak", "address","email@email.pl","666666666");
        Client client3 = new Client("Jan","Nowak", "address","email@email.pl","666666666");
        Client client4 = new Client("Wojtek","Nowak", "address","email@email.pl","666666666");

        Library library = new Library();
        library.addClient(client);
        library.addClient(client2);
        library.addClient(client3);
        library.addClient(client4);

        System.out.println(library.showListOfClients());

        Assert.assertTrue(true);
    }
    @Test
    public void shouldShowListOfBooks(){
        Book book = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Book book2 = new Book("AAAAA", "testoviron", 2011, 453, Category.NOVEL );
        Book book3 = new Book("co", "testoviron", 2011, 453, Category.NOVEL );
        Library library = new Library();

        library.addBook(book);
        library.addBook(book2);
        library.addBook(book3);
        System.out.println(library.showListOfBooks());

        Assert.assertTrue(true);
    }
    @Test
    public void shouldShowBorrowedList(){
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        Client client2 = new Client("Mateusz","Nowak", "address","email@email.pl","666666666");
        Client client3 = new Client("Jan","Nowak", "address","email@email.pl","666666666");
        Client client4 = new Client("Wojtek","Nowak", "address","email@email.pl","666666666");

        Book book = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Book book2 = new Book("AAAAA", "testoviron", 2011, 453, Category.NOVEL );
        Book book3 = new Book("co", "testoviron", 2011, 453, Category.NOVEL );

        Library library = new Library();
        library.addClient(client);
        library.addClient(client2);
        library.addClient(client3);
        library.addClient(client4);

        library.addBook(book);
        library.addBook(book2);
        library.addBook(book3);

        library.borrowBook(client, book);
        library.borrowBook(client2, book2);
        library.borrowBook(client3, book3);

        System.out.println(library.showBorrowed());

        Assert.assertTrue(true);
    }

    @Test
    public void shouldCheckIfBorrowed() {
        //Given
        Library library = new Library();
        Book book = new Book("AAA","AAA",1988,255,Category.FANTASY);
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        //When
        library.borrowBook(client, book);
        //Than
        assertTrue(library.isBorrowed(book));
        assertEquals(client.getBorrowedBooksIDs().get(0), Integer.valueOf(book.getBookID()));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void shouldNotBorrowABookMoreThanOnce(){
        //Given
        Library library = new Library();
        Book book = new Book("AAA","AAA",1988,255,Category.FANTASY);
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        Client client2 = new Client("Heniek","Nowak", "address","email@email.pl","666666666");
        //When
        library.borrowBook(client, book);
        library.borrowBook(client2, book);

    }
    @Test
    public void shouldBorrowBook(){
        //Given
        Library library = new Library();
        Book book = new Book("AAA","AAA",1988,255,Category.FANTASY);
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        //When
        library.addClient(client);
        library.addBook(book);
        library.borrowBook(client, book);
        //Than
        assertTrue(library.isBorrowed(book));
        assertTrue(client.getBorrowedBooksIDs().contains(book.getBookID()));
        assertTrue(library.getBorrowed().containsKey(client.getNumber()));

    }

    @Test
    public void shouldReturnBook() {
        //Given
        Library library = new Library();
        Book book = new Book("AAA","AAA",1988,255,Category.FANTASY);
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        library.borrowBook(client, book);
        //When

        book.setBorrowExpirationDate(LocalDate.now().minusMonths(1));
        library.returnBook(client, book);
        //Than
        assertFalse(library.isBorrowed(book));
        assertEquals(0, client.getBorrowedBooksIDs().size());
    }

    @Test
    public void shouldSaveLibraryToFile() throws IOException {
        //Given
        Library library = new Library();
        Library library2;
        Book book = new Book("AAA","AAA",1988,255,Category.FANTASY);
        Book book4 = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Book book2 = new Book("AAAAA", "testoviron", 2011, 453, Category.NOVEL );
        Book book3 = new Book("co", "testoviron", 2011, 453, Category.NOVEL );
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        Client client2 = new Client("Mateusz","Nowak", "address","email@email.pl","666666666");
        Client client3 = new Client("Jan","Nowak", "address","email@email.pl","666666666");
        Client client4 = new Client("Wojtek","Nowak", "address","email@email.pl","666666666");


        library.addBook(book);
        library.addBook(book2);
        library.addBook(book3);
        library.addBook(book4);
        library.addClient(client);
        library.addClient(client2);
        library.addClient(client3);
        library.addClient(client4);
        library.borrowBook(client, book);
        library.borrowBook(client2, book2);
        library.borrowBook(client3, book3);
        //When
        library.save();
        library2 = Library.load(Boot.findMostRecent());
        //Than
        assertEquals(library.getNoOfClients(), library2.getNoOfClients());
        assertEquals(library.getBorrowed(), library2.getBorrowed());
        assertEquals(library.getNoOfBooks(), library2.getNoOfBooks());
    }

    @Test
    public void shouldLoadLibrary() throws FileNotFoundException {
        //Given
        String fileName = "2020-02-16 13_43_01.json";
        //When
        Library library = Library.load(fileName);
        //Than
        assertEquals(library.getNoOfBooks(),  Library.load(fileName).getNoOfBooks());
        assertEquals(library.getNoOfClients(),  Library.load(fileName).getNoOfClients());
        assertEquals(library.getBookList().size(),  Library.load(fileName).getBookList().size());
        assertEquals(library.getBorrowed(),  Library.load(fileName).getBorrowed());

    }

    @Test
    public void shouldFindBooksByTitle() throws FileNotFoundException {
        //given
        String expected = "Title: TEST | Author: testoviron | ID: 374769592 | Category: NOVEL\n";
        Library library = Library.load("2020-02-16 13_43_01.json");
        //When
        String result = library.findBooks("TEST");
        //Than
        assertEquals(expected,result);
    }
    @Test
    public void shouldFindBooksByAuthor() throws FileNotFoundException {
        //given
        String expected = "Title: AAAAA | Author: testoviron | ID: 289962765 | Category: NOVEL\n" +
                "Title: co | Author: testoviron | ID: 133967269 | Category: NOVEL\n" +
                "Title: TEST | Author: testoviron | ID: 374769592 | Category: NOVEL\n";
        Library library = Library.load("2020-02-16 13_43_01.json");
        //When
        String result = library.findBooksByAuthor("testoviron");
        //Than
        assertEquals(expected,result);
    }
    @Test
    public void shouldFindBooksByCategory() throws FileNotFoundException {
        //given
        String expected = "Title: AAAAA | Author: testoviron | ID: 289962765 | Category: NOVEL\n" +
                "Title: co | Author: testoviron | ID: 133967269 | Category: NOVEL\n" +
                "Title: TEST | Author: testoviron | ID: 374769592 | Category: NOVEL\n";
        Library library = Library.load("2020-02-16 13_43_01.json");
        //When
        String result = library.findBooks(Category.NOVEL);
        //Than
        assertEquals(expected,result);
    }
}