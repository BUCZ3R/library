import library.Client;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClientTest {

    @Test
    public void shouldCreateClient(){
        //given
        String name = "Jan";
        String lastName = "Kowalski";
        String address = "Lublin, ul.Jakastam 10/10";
        String email = "Jankow@aaa.pl";
        String phoneNumber = "666666666";
        //when
        Client client = new Client(name,lastName, address, email,phoneNumber);
        //than
        assertEquals(name,client.getName());
        assertEquals(lastName,client.getLastName());
        assertEquals(address,client.getAddress());
        assertEquals(email,client.getEmail());
        assertEquals(phoneNumber,client.getPhoneNumber());
    }

    @Test
    public void shouldSetLastName() {
        //given
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        String lastName = "Kowalski";
        //when
        client.setLastName(lastName);
        //than
        assertEquals(lastName, client.getLastName());
    }

    @Test
    public void shouldSetAddress() {
        //given
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        String address = "ul. Bylejaka 10/5";
        //when
        client.setAddress(address);
        //than
        assertEquals(address, client.getAddress());
    }

    @Test
    public void shouldSetEmail() {
        //given
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        String email = "123@123.pl";
        //when
        client.setEmail(email);
        //than
        assertEquals(email, client.getEmail());
    }

    @Test
    public void shouldSetPhoneNumber() {
        //given
        Client client = new Client("Zenek","Nowak", "address","email@email.pl","666666666");
        String phoneNumber = "777777777";
        //when
        client.setPhoneNumber(phoneNumber);
        //than
        assertEquals(phoneNumber, client.getPhoneNumber());
    }
}