import library.Book;
import library.Category;
import library.Client;
import library.Library;
import org.junit.Test;
import tools.SendReminder;

public class SendReminderTest {

    @Test
    public void shouldSendMail(){
        Client client = new Client("Zenek","Nowak", "address","parkarop@gmail.com","666666666");
        Book book = new Book("AAA","AAA",1988,255, Category.FANTASY);
        Book book4 = new Book("TEST", "testoviron", 2011, 453, Category.NOVEL );
        Book book2 = new Book("AAAAA", "testoviron", 2011, 453, Category.NOVEL );
        Library library = new Library();

        library.borrowBook(client, book);
        library.borrowBook(client, book4);
        library.borrowBook(client, book2);


    }

}