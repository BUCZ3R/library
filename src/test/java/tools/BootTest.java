package tools;

import library.Library;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;

public class BootTest {

    @Test
    public void shouldDeleteFile() throws IOException {
        Library library = new Library();
        library.save();
        File file = new File(Boot.getFileList().get(Boot.getFileList().size()-1));
        Boot.deleteFile(Boot.getFileList().size()-1);
        assertFalse(file.exists());

    }
}