package tools;

import com.google.gson.Gson;
import library.Library;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Boot {
    private static List<String> fileList = new ArrayList<>();

    static {
        addFiles();
    }

    private static void addFiles() {
        File f = new File(".\\");
        String[] list = f.list();
        if (list != null) {
            for (String s : list) {
                if (s.contains(".json")) {
                    fileList.add(s);
                }
            }
        }
    }

    public static void deleteFile(int id) throws IOException {
        Path path = Paths.get(fileList.get(id));
        Files.deleteIfExists(path);
        System.out.println("File deleted: " + fileList.get(id));
        fileList.remove(id);

    }

    public static void listFiles() throws FileNotFoundException {
        if (fileList.isEmpty()) {
            throw new FileNotFoundException("Couldn't find any Library files.");
        } else {
            for (int i = 0; i < fileList.size(); i++) {
                System.out.println(i + 1 + ". " + fileList.get(i));
            }
        }
    }

    public static String findMostRecent() throws FileNotFoundException {
        if (fileList.isEmpty()) {
            throw new FileNotFoundException("Couldn't find any Library files.");
        }
        LocalDateTime recent = LocalDateTime.parse(fileList.get(0).substring(0, 19)
                .replace('_', ':')
                .replace(' ', 'T'));
        int index = 0;

        List<LocalDateTime> dates = new ArrayList<>();
        for (int i = 0; i < fileList.size(); i++) {
            String date = fileList.get(i).substring(0, 19)
                    .replace('_', ':')
                    .replace(' ', 'T');
            dates.add(i, LocalDateTime.parse(date));
        }
        for (LocalDateTime dateTime : dates) {
            if (dateTime.isAfter(recent)) {
                recent = dateTime;
                index = dates.indexOf(dateTime);
            }
        }

        return fileList.get(index);

    }

    public static Library boot() throws FileNotFoundException {
        Gson gson = new Gson();
        String fileName;
        try {
            fileName = findMostRecent();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Created new Library");
            return new Library();
        }
        System.out.println("Library form file: " + fileName);
        return gson.fromJson(new FileReader(fileName), Library.class);
    }

    public static List<String> getFileList() {
        return fileList;
    }
}
