package tools;

import library.Book;
import library.Client;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import menu.MainMenu;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class SendReminder implements Runnable {

    private final static String USERNAME = "librarytest12356@gmail.com";
    private final static String PASSWORD = "qwe!@#AA";

    public static void send(List<ReminderData> dataList) {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.gmail.com");
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(USERNAME, PASSWORD);
            }
        });
        System.out.println("Session created...");
        for(ReminderData rd: dataList){
            String email = rd.getClient().getEmail();
            List<Integer> booksIDs = rd.getBooksIDs();
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(USERNAME));
                message.setRecipient(Message.RecipientType.TO,
                        new InternetAddress(email));

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Dear ").append(rd.getClient().getName()).append(", \n");
                stringBuilder.append("Please return books with these IDs: \n");
                for (int i : booksIDs) {
                    stringBuilder.append(i).append("\n");
                }
                stringBuilder.append("Return these books as quickly as you can. In other case you will pay a fine.");
                message.setSubject("library.Library - Please retrun these books!");
                message.setText(stringBuilder.toString());

                Transport.send(message);

                System.out.println("Message sent to: " + rd.getClient().getEmail());

            } catch (MessagingException e) {
                e.printStackTrace();
            }

        }

    }
    @SneakyThrows
    public void run() {
        List<ReminderData> dataList = new ArrayList<>();
        Map<Integer, List<Integer>> borrowed = MainMenu.getLibrary().getBorrowed();
        for (Map.Entry<Integer, List<Integer>> entry : borrowed.entrySet()){
            List<Integer> booksToReturnIDs = new ArrayList<>();
            int clientID = entry.getKey();
            Client client = MainMenu.getLibrary().findClient(clientID);
            List<Integer> booksIDs = entry.getValue();
            for (int bookId: booksIDs) {
                Book book = MainMenu.getLibrary().findBook(bookId);
                if (LocalDate.now().isAfter(book.getBorrowExpirationDate())){
                    booksToReturnIDs.add(book.getBookID());
                }
            }
            if (!booksToReturnIDs.isEmpty()){
                dataList.add(new ReminderData(client, booksToReturnIDs));
            }
        }
        send(dataList);
    }

    @Getter
    @AllArgsConstructor
    private class ReminderData{
        private Client client;
        private List<Integer> booksIDs;
    }
}