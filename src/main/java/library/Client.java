package library;

import lombok.Getter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Client {

    private String name;
    private String lastName;
    private int number;
    private String address;
    private String email;
    private String phoneNumber;
    private List<Integer> borrowedBooksIDs = new ArrayList<>();

    public Client(String name, String lastName, String address, String email, String phoneNumber) {
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
        int number = (int)(((LocalDateTime.now().hashCode()*(Math.random()*10)))/(21-Math.random()*10));
        if(number < 0){ number*=-1; }
        this.number = number;

    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
