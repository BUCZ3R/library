package library;

public enum Category {
    NOVEL,
    FANTASY,
    HORROR
}
