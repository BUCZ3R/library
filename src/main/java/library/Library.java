package library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import tools.Boot;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Getter
public class Library {

    private List<Book> bookList = new ArrayList<>();
    private List<Client> clientList = new ArrayList<>();
    private Map<Integer, List<Integer>> borrowed = new HashMap<>();
    public int noOfClients = 0;
    public int noOfBooks = 0;

    public void addClient(Client client) {
        int counter = 0;
        for (Client c : clientList) {
            if (c.getNumber() == client.getNumber()) {
                counter++;
            }
        }
        if (counter == 0) {
            clientList.add(client);
            noOfClients++;
            System.out.println("library.Client added.");
        } else {
            System.out.println("library.Client already on the list.");
        }
    }

    public void removeClient(Client client) {
        if (clientList.contains(client)) {
            clientList.remove(client);
            noOfClients--;
            System.out.println("library.Client removed");
        } else {
            System.out.println("library.Client not on the list.");
        }
    }

    public Client findClient(int clientID) throws Exception {
        for (Client c : clientList) {
            if (c.getNumber() == clientID) {
                return c;
            }
        }
        throw new Exception("Client not found.");
    }

    public void removeClientByNumber(int number) throws Exception {
        Client client = findClient(number);
        if (client == null) System.out.println("Client not found!");
        else {
            clientList.remove(client);
            System.out.println("Client removed!");
        }
    }


    public void addBook(Book book) {
        long count = bookList.stream().filter(book1 -> book1.getBookID() == book.getBookID()).count();
        if (count == 1) {
            System.out.println("This book is already inside library!");
        } else {
            bookList.add(book);
            noOfBooks++;
            System.out.println("library.Book " + book.getTitle()
                    + " by " + book.getAuthor() + " has been added!");
        }
    }

    public void removeBook(Book book) {
        Optional<Book> foundBook = bookList.stream()
                .filter(book1 -> book1.getBookID() == book.getBookID())
                .findAny();
        if (foundBook.isPresent()) {
            bookList.remove(book);
            noOfBooks--;
            System.out.println("library.Book has been removed!");
        } else {
            System.out.println("There is no such a book!");
        }
    }

    public void borrowBook(Client client, Book book) {
        long count = clientList.stream()
                .filter(c -> c.getNumber() == client.getNumber())
                .count();

        if (count == 0) {
            clientList.add(client);
        }
        if (client.getBorrowedBooksIDs().size() > 2) {
            throw new UnsupportedOperationException("Can't borrow more books.");
        } else if (!isBorrowed(book)) {
            LocalDate today = LocalDate.now();
            client.getBorrowedBooksIDs().add(book.getBookID());
            book.setBorrowExpirationDate(today.plusWeeks(2));
            borrowed.put(client.getNumber(), client.getBorrowedBooksIDs());
            System.out.println("Book " + book.getTitle() + " by " + book.getAuthor() + " has been borrowed!");
        } else {
            throw new UnsupportedOperationException("Book unavailable.");
        }

    }

    public void returnBook(Client client, Book book) {
        if (!client.getBorrowedBooksIDs().contains(book.getBookID())) {
            throw new UnsupportedOperationException("User didn't borrow this book.");
        } else {

            client.getBorrowedBooksIDs().remove(Integer.valueOf(book.getBookID()));

            if (LocalDate.now().isAfter(book.getBorrowExpirationDate())) {
                System.out.println("Return date was exceeded. Expiration date: " + book.getBorrowExpirationDate());
            }
            book.setBorrowExpirationDate(null);
        }
        if (client.getBorrowedBooksIDs().size() == 0) {
            borrowed.remove(client.getNumber());
        }
        System.out.println("Book returned!");

    }

    public boolean isBorrowed(Book book) {
        return book.getBorrowExpirationDate() != null;
    }

    public void save() throws IOException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd' 'HH_mm_ss");
        String fileName = LocalDateTime.now().format(format);
        File file = new File(fileName + ".json");
        FileWriter writer = new FileWriter(file);
        gson.toJson(this, writer);
        writer.flush();
        writer.close();
        System.out.println("Libary saved to: " + fileName + ".json");
        Boot.getFileList().add(fileName + ".json");

    }

    public static Library load(String fileName) throws FileNotFoundException {
        Gson gson = new Gson();
        System.out.println("Library loaded from file: " + fileName);
        return gson.fromJson(new FileReader(fileName), Library.class);
    }

    public String showBorrowed() throws NoSuchElementException {
        try {
            StringBuilder toShow = new StringBuilder();
            String showClient;
            String showBook;
            for (Map.Entry<Integer, List<Integer>> entry : borrowed.entrySet()) {
                int clientID = entry.getKey();
                List<Integer> booksIDs = entry.getValue();
                Optional<Client> foundClient = clientList.stream()
                        .filter(client -> client.getNumber() == clientID)
                        .findAny();
                if (foundClient.isEmpty()) throw new NoSuchElementException();
                Client client = foundClient.get();
                showClient = "\nName: " + client.getName()
                        + " Last name: " + client.getLastName()
                        + " ID: " + client.getNumber();
                toShow.append(showClient);
                for (Integer booksID : booksIDs) {
                    Optional<Book> foundBook = bookList.stream()
                            .filter(book -> book.getBookID() == booksID)
                            .findAny();
                    if (foundBook.isEmpty()) throw new NoSuchElementException();
                    Book book = foundBook.get();
                    showBook = "\n  " + "Title: " + book.getTitle()
                            + " Author: " + book.getAuthor()
                            + " ID: " + book.getBookID();
                    toShow.append(showBook);
                }
            }
            return toShow.toString();
        } catch (NoSuchElementException e) {
            System.out.println("Błąd wczytywania!");
            return null;
        }

    }

    public String showListOfBooks() {
        StringBuilder toShow = new StringBuilder();
        String showBook;
        for (Book book : bookList) {
            showBook = "Title: " + book.getTitle() + " | Author: " + book.getAuthor() + " | ID: " + book.getBookID() + " | Category: " + book.getCategory();
            toShow.append(showBook).append("\n");
        }
        return toShow.toString();
    }

    public String showAvailableBooks() {
        StringBuilder toShow = new StringBuilder();
        String showBook;
        for (Book book : bookList) {
            if (book.getBorrowExpirationDate() == null) {
                showBook = "Title: " + book.getTitle() + " | Author: " + book.getAuthor() + " | ID: " + book.getBookID() + " | Category: " + book.getCategory();
                toShow.append(showBook).append("\n");
            }
        }
        return toShow.toString();
    }

    public String showListOfClients() {
        StringBuilder toShow = new StringBuilder();
        String showClient;
        for (Client client : clientList) {
            showClient = "Name: " + client.getName()
                    + " Last name: " + client.getLastName()
                    + " ID: " + client.getNumber();
            toShow.append(showClient).append("\n");
        }
        return toShow.toString();
    }

    public Book findBook(int bookID) {
        Optional<Book> foundBook = bookList.stream()
                .filter(book -> book.getBookID() == bookID)
                .findFirst();
        return foundBook.orElse(null);
    }

    public String findBooks(String title) {
        List<Book> list;
        list = bookList.stream().filter(e -> e.getTitle().equals(title)).collect(Collectors.toList());
        StringBuilder toShow = new StringBuilder();
        String showBook;
        for (Book book : list) {
            showBook = "Title: " + book.getTitle() + " | Author: " + book.getAuthor()
                    + " | ID: " + book.getBookID() + " | Category: " + book.getCategory();
            toShow.append(showBook).append("\n");
        }
        return toShow.toString();
    }

    public String findBooks(Category category) {
        List<Book> list;
        list = bookList.stream().filter(e -> e.getCategory().equals(category)).collect(Collectors.toList());
        StringBuilder toShow = new StringBuilder();
        String showBook;
        for (Book book : list) {
            showBook = "Title: " + book.getTitle() + " | Author: " + book.getAuthor()
                    + " | ID: " + book.getBookID() + " | Category: " + book.getCategory();
            toShow.append(showBook).append("\n");
        }
        return toShow.toString();

    }

    public String findBooksByAuthor(String author) {
        List<Book> list;
        list = bookList.stream().filter(e -> e.getAuthor().equals(author)).collect(Collectors.toList());
        StringBuilder toShow = new StringBuilder();
        String showBook;
        for (Book book : list) {
            showBook = "Title: " + book.getTitle() + " | Author: " + book.getAuthor()
                    + " | ID: " + book.getBookID() + " | Category: " + book.getCategory();
            toShow.append(showBook).append("\n");
        }
        return toShow.toString();
    }


}
