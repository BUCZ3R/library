package library;

import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Getter

public class Book {

    private int bookID;
    private String title;
    private String author;
    private int publicationYear;
    private int numberOfPages;
    private LocalDate borrowExpirationDate = null;
    private Category category;

    public Book(String title, String author, int publicationYear, int numberOfPages, Category category) {
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.numberOfPages = numberOfPages;
        this.category = category;
        int number = (int)(((LocalDateTime.now().hashCode()*(Math.random()*10)))/(21-Math.random()*10));
        if(number < 0){ number*=-1; }
        bookID = number;

    }

    public void setBorrowExpirationDate(LocalDate borrowExpirationDate) {
        this.borrowExpirationDate = borrowExpirationDate;
    }
    public void setBookID(int id){
        this.bookID = id;
    }

}
