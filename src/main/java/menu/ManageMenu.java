package menu;
import library.Library;
import tools.Boot;
import java.util.Scanner;

public class ManageMenu {

    private static Scanner scanner = new Scanner(System.in);

    public static void show() throws Exception {

        String toShow = "1 - Save library status to file\n\n" +
                "2 - Load library status from file\n\n" +
                "3 - Delete file\n\n" +
                "4 - Back\n\n";
        System.out.println(toShow);
        showManageMenu();
    }

    private static void showManageMenu() throws Exception {
        Library library = Boot.boot();
        int choice = scanner.nextInt();
        switch (choice){
            case 1:
                MainMenu.clear();
                library.save();
                show();
                break;
            case 2:
                MainMenu.clear();
                Boot.listFiles();
                System.out.println("Choose file to load : ");
                int index = scanner.nextInt();
                library = Library.load(Boot.getFileList().get(index-1));
                library.save();
                show();
                break;
            case 3:
                MainMenu.clear();
                deleteFile();
                show();
                break;
            case 4:
                MainMenu.clear();
                MainMenu.show();
                break;
            default:
                System.out.println("Invalid input.");
                break;
        }
    }

    private static void deleteFile() throws Exception {
        Boot.listFiles();
        System.out.println("Choose file to delete, type 0 to abort: ");
        int fileIndex = scanner.nextInt();
        if (fileIndex == 0 ){
            show();
        }else if(Boot.getFileList().size()<fileIndex || fileIndex<0){
            System.out.println("Invalid input");
            show();
        }else {
            Boot.deleteFile(fileIndex-1);
        }
    }




}
