package menu;

import library.Book;
import library.Category;
import library.Client;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class BooksMenu {


    static void show() throws Exception {
        String toShow = "1 - Borrow Book \n \n" +
                "2 - Return Book \n \n" +
                "3 - Show all available books \n \n" +
                "4 - Show list of borrowed books \n \n" +
                "5 - Show all books \n \n" +
                "6 - Find book\n \n"+
                "7 - Add book to Library \n \n" +
                "8 - Remove book from Library \n \n" +
                "9 - Return to Main Menu \n \n";
        System.out.print(toShow);
        Scanner scanner = new Scanner(System.in);
        int chosenOption = scanner.nextInt();
        switch (chosenOption){
            case 1:
                MainMenu.clear();
                borrowBook();
                break;
            case 2:
                MainMenu.clear();
                returnBook();
                break;
            case 3:
                MainMenu.clear();
                System.out.print(MainMenu.getLibrary().showAvailableBooks());
                System.out.println();
                show();
                break;
            case 4:
                MainMenu.clear();
                System.out.println(MainMenu.getLibrary().showBorrowed());
                System.out.println();
                show();
                break;
            case 5:
                MainMenu.clear();
                System.out.println(MainMenu.getLibrary().showListOfBooks());
                show();
                break;
            case 6:
                MainMenu.clear();
                findBookMenu();
                break;
            case 7:
                MainMenu.clear();
                addBook();
                break;
            case 8:
                MainMenu.clear();
                removeBook();
                break;
            case 9:
                MainMenu.clear();
                MainMenu.show();
                break;
            default:
                System.out.println("Invalid input!");
                show();
        }
    }

    private static void findBookMenu(){
        String toShow = "1 - Find by author \n \n" +
                "2 - Find by title \n \n" +
                "3 - Find By cathegory \n \n"+
                "4 - Back \n \n";
        System.out.print(toShow);
        Scanner scanner = new Scanner(System.in);
        int chosenOption = scanner.nextInt();
        switch (chosenOption){
            case 1:
                scanner.nextLine();
                System.out.println("Input author: ");
                String author = scanner.nextLine();
                System.out.println(MainMenu.getLibrary().findBooksByAuthor(author));
                findBookMenu();
                break;
            case 2:
                scanner.nextLine();
                System.out.println("Input title: ");
                String title = scanner.nextLine();
                System.out.println(MainMenu.getLibrary().findBooks(title));
                findBookMenu();
                break;
            case 3:
                MainMenu.clear();
                findByCategoryMenu();
                findBookMenu();
            case 4:
                try {
                    BooksMenu.show();
                }catch (Exception e){
                    e.printStackTrace();
                }
        }
    }
    private static void borrowBook() throws Exception {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Type client ID: ");
            int clientID = scanner.nextInt();
            MainMenu.clear();
            Client client = MainMenu.getLibrary().findClient(clientID);
            System.out.println();
            if (client == null) {
                System.out.println("Client not found!");
                System.out.println();
            } else {
                System.out.println("Type Book ID: ");
                int bookID = scanner.nextInt();
                Book book = MainMenu.getLibrary().findBook(bookID);
                System.out.println();
                if (book != null) {
                    MainMenu.getLibrary().borrowBook(client, book);
                    System.out.println();
                }
            }
            show();
        }catch (InputMismatchException e){
            MainMenu.clear();
            System.out.println("Invalid input!");
            System.out.println();
            borrowBook();
        } catch (Exception e ){
            MainMenu.clear();
            System.out.println("Client not found!");
            System.out.println();
            show();
        }
    }
    private static void returnBook() throws Exception {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Type client ID: ");
            int clientID = scanner.nextInt();
            MainMenu.clear();
            Client client = MainMenu.getLibrary().findClient(clientID);
            System.out.println();
            if (client == null) {
                System.out.println("Client not found!");
                System.out.println();
                show();
            } else {
                System.out.println("Type Book ID: ");
                int bookID = scanner.nextInt();
                MainMenu.clear();
                Optional<Book> foundBook = MainMenu.getLibrary().getBookList().stream().filter(book -> book.getBookID() == bookID).findFirst();
                if (foundBook.isPresent()) {
                    MainMenu.getLibrary().returnBook(client, foundBook.get());
                    System.out.println();
                    show();
                } else {
                    System.out.println("Book not found!");
                    System.out.println();
                    returnBook();
                }
            }
        }catch (InputMismatchException e){
            MainMenu.clear();
            System.out.println("Invalid input!");
            System.out.println();
            returnBook();
        }
    }
    private static void addBook() throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type title of the book:  ");
        String title = scanner.nextLine();
        MainMenu.clear();
        System.out.println("Type author of the book:  ");
        String author = scanner.nextLine();
        MainMenu.clear();
        try {
            System.out.println("Type publication year of the book:  ");
            int publicationYear = scanner.nextInt();
            MainMenu.clear();
            System.out.println("Type number of pages:  ");
            int numberOfPages = scanner.nextInt();
            MainMenu.clear();

            List<String> enumNames = Stream.of(Category.values())
                    .map(Enum::name)
                    .collect(Collectors.toList());
            System.out.println("Choose category by typing its number: ");
            int inputNumber;
            do {
                for (int i = 0; i < enumNames.size(); i++) {
                    System.out.println(i + 1 + ". " + enumNames.get(i));
                }
                inputNumber = scanner.nextInt();
                MainMenu.clear();
                if (inputNumber > enumNames.size() || inputNumber < 0) System.out.println("Invalid number!");
            } while (inputNumber > enumNames.size() || inputNumber < 0);

            Category[] categories = Category.values();
            int count = 1;
            Category chosenCategory = null;
            for (Category category : categories) {
                if (count == inputNumber) {
                    chosenCategory = category;
                    break;
                }
                count++;
            }
            Book createdBook = new Book(title, author, publicationYear, numberOfPages, chosenCategory);
            MainMenu.getLibrary().addBook(createdBook);
            MainMenu.clear();
            System.out.println("Book " + createdBook.getTitle() + " by " + createdBook.getAuthor() + " has been added to Library!");
            System.out.println();
            show();
        } catch (InputMismatchException e){
            MainMenu.clear();
            System.out.println("Invalid input!");
            System.out.println();
            addBook();
        }
    }
    private static void removeBook() throws Exception {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type title of the book:  ");
        String title1 = scanner.nextLine();
        MainMenu.clear();
        System.out.println("Type author of the book:  ");
        String author1 = scanner.nextLine();
        MainMenu.clear();
        try {
            System.out.println("Type publication year of the book:  ");
            int publicationYear1 = scanner.nextInt();
            MainMenu.clear();
            System.out.println("Type number of pages:  ");
            int numberOfPages1 = scanner.nextInt();
            MainMenu.clear();
            Optional<Book> foundBook = MainMenu.getLibrary().getBookList().stream()
                    .filter(book -> book.getTitle().equals(title1))
                    .filter(book -> book.getAuthor().equals(author1))
                    .filter(book -> book.getPublicationYear()==publicationYear1)
                    .filter(book -> book.getNumberOfPages()==numberOfPages1).findFirst();
            if (foundBook.isPresent()){
                Book book = foundBook.get();
                if (book.getBorrowExpirationDate()!=null){
                    System.out.println("The selected book is borrowed! Are u sure to remove this book form Library? (y/n)");
                    String answer = scanner.nextLine().toLowerCase();
                    MainMenu.clear();
                    if (answer.equals("y")){
                        MainMenu.getLibrary().removeBook(book);
                        System.out.println("Book "+book.getTitle()+" by "+book.getAuthor()+" has been removed from Library!");
                        System.out.println();
                        show();
                    } else if (answer.equals("n")){
                        System.out.println("Action cancelled");
                        System.out.println();
                        show();
                    }
                } else {
                    MainMenu.getLibrary().removeBook(book);
                    System.out.println("Book "+book.getTitle()+" by "+book.getAuthor()+" has been removed from Library!");
                    System.out.println();
                    show();
                }
            } else {
                System.out.println("Book not found!");
                System.out.println();
                show();
            }
        } catch (InputMismatchException e){
            MainMenu.clear();
            System.out.println("Invalid input!");
            System.out.println();
            removeBook();
        }
    }
    public static void findByCategoryMenu() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose category, type 0 to go back: ");
        Category[] categories = Category.values();
        for (int i = 0; i <categories.length ; i++) {
            System.out.println(i+1 + ". "+categories[i]);
        }
        int choice = scanner.nextInt();
        if (choice == 0){
            try{
        show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if(choice>0 && choice<categories.length){
            System.out.println("Searching for category: "+categories[choice-1]);
            System.out.println(MainMenu.getLibrary().findBooks(categories[choice-1]));
        }else{
            MainMenu.clear();
            System.out.println("Invalid input");
            findByCategoryMenu();
        }
    }
}
