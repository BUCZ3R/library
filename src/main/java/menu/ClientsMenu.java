package menu;

import library.Client;
import java.util.Scanner;

public class ClientsMenu {

    private static String lastName;
    private static String address;
    private static String email;
    private static String phoneNumber;
    private static int number;

    private  static Scanner scanner = new Scanner(System.in);
    public static void show() throws Exception {

        clientsMainMenu();

        int chosenOption = Integer.parseInt(scanner.nextLine());
        switch (chosenOption) {
            case 1:
                addClientMenu();
                break;
            case 2:
                System.out.println("Client ID: ");
                number = Integer.parseInt(scanner.nextLine());
                editClient();
                break;
            case 3:
                removeClientMenu();
                break;
            case 4:
                MainMenu.clear();
                System.out.println(MainMenu.getLibrary().showListOfClients());
                show();
                break;
            case 5:
                MainMenu.clear();
                try {
                    MainMenu.show();
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            default:
                System.out.println("Invalid input.");
                show();
        }
    }

    public static void clientsMainMenu(){
        String toShow = "1 - Add Client \n \n" +
                "2 - Edit Client Data \n \n" +
                "3 - Remove Client \n \n" +
                "4 - Show Client List \n \n" +
                "5 - Return to Main Menu \n \n";
        System.out.print(toShow);
    }

    public static void addClientMenu(){
        MainMenu.clear();
        System.out.println("Name: ");
        String name = scanner.nextLine();
        System.out.println("Last name: ");
        lastName = scanner.nextLine();
        System.out.println("Address: ");
        address = scanner.nextLine();
        System.out.println("Email: ");
        email = scanner.nextLine();
        System.out.println("phoneNumber: ");
        phoneNumber = scanner.nextLine();
        MainMenu.getLibrary().addClient(new Client(name,lastName,address,email,phoneNumber));
    }

    public static void removeClientMenu() throws Exception {
        MainMenu.clear();
        System.out.println("Give me client ID: ");
        int clientID = scanner.nextInt();
        MainMenu.clear();
        MainMenu.getLibrary().removeClientByNumber(clientID);
        System.out.println();
        show();
    }


    private static void editClient() throws Exception{

        Client client = MainMenu.getLibrary().findClient(number);
        System.out.println("Choose data to edit: \n1.Last name\n2.Address\n3.Email\n4.Phone number\n5.Back");
        int operation = Integer.parseInt(scanner.nextLine());
        switch (operation){
            case 1:
                MainMenu.clear();
                System.out.println("New last name: ");
                lastName = scanner.nextLine();
                client.setLastName(lastName);
                editClient();
            case 2:
                MainMenu.clear();
                System.out.println("New address: ");
                address = scanner.nextLine();
                client.setAddress(address);
                editClient();
            case 3:
                MainMenu.clear();
                System.out.println("New email: ");
                email = scanner.nextLine();
                client.setEmail(email);
                editClient();
            case 4:
                MainMenu.clear();
                System.out.println("New phone number");
                phoneNumber = scanner.nextLine();
                client.setPhoneNumber(phoneNumber);
                editClient();
            case 5:
                show();
            default:
                System.out.println("Invalid input.");
                show();

        }
    }
}
