package menu;

import library.Library;
import java.util.Scanner;

public class MainMenu {

    private static Library library;

    public static void show() throws Exception {
        StringBuilder toShow = new StringBuilder();
        System.out.println("Welcome to LibraryOS! version pre-build 0.0.5v ");
        System.out.println("Select an option by choosing its number:");
        toShow.append("1 - Books \n \n");
        toShow.append("2 - Clients \n \n");
        toShow.append("3 - Manage \n \n");
        toShow.append("4 - Exit \n \n");
        System.out.print(toShow.toString());
        Scanner scanner = new Scanner(System.in);
        int chosenOption = scanner.nextInt();
        switch (chosenOption) {
            case 1:
                MainMenu.clear();
                BooksMenu.show();
                break;
            case 2:
                MainMenu.clear();
                ClientsMenu.show();
                break;
            case 3:
                MainMenu.clear();
                ManageMenu.show();
                break;
            case 4:
                library.save();
                System.exit(0);
        }

    }

    public static void clear() {
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

    public static Library getLibrary() {
        return library;
    }

    public static void setLibrary(Library library) {
        MainMenu.library = library;
    }
}
