import library.Book;
import library.Category;
import library.Client;
import library.Library;
import menu.MainMenu;
import tools.Boot;
import tools.SendReminder;


public class Main {
    public static void main(String[] args) throws Exception {

        Library library = Boot.boot();
        SendReminder sendReminder = new SendReminder();
        Thread thread = new Thread(sendReminder);
//        Book book4 = new Book("TEST", "testoviron", 2011,
//                    453, Category.NOVEL );
//        Client client = new Client("Zenek","Nowak",
//                                    "address","parkarop@gmail.com",
//                                    "666666666");
//        book4.setBookID(1);
//        client.setNumber(1);
//        library.addBook(book4);
//        library.addClient(client);
//        library.borrowBook(client, book4);
//        book4.setBorrowExpirationDate(book4.getBorrowExpirationDate().minusMonths(2));
        MainMenu.setLibrary(library);
        thread.start();

        try {
            MainMenu.show();
        }catch (Exception e){
            e.printStackTrace();
        }
        MainMenu.clear();
        MainMenu.show();

    }
}
